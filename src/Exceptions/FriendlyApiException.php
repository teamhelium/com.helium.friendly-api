<?php

namespace Helium\FriendlyApi\Exceptions;

use Exception;
use Throwable;

class FriendlyApiException extends Exception
{
	/**
	 * @description General exception for failed FriendlyApi requests.
	 * FriendlyApiException should only be thrown as a wrapper around a previous
	 * exception, which is a required argument for debugging purposes. If an
	 * exception relating to FriendlyAPi should be thrown without a previous
	 * exception, create a custom exception class.
	 * @param Throwable $previous
	 */
	public function __construct(Throwable $previous)
	{
		$message = 'Could not complete the request. ';
		$message .= 'See previous exception for more information.';

		parent::__construct($message, 0, $previous);
	}
}