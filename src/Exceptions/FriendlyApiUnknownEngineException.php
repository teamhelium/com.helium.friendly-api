<?php

namespace Helium\FriendlyApi\Exceptions;

use Exception;
use Throwable;

class FriendlyApiUnknownEngineException extends Exception
{
	/**
	 * @description An unknown FriendlyApi engine was called
	 * @param string $key
	 */
	public function __construct(string $key)
	{
		$message = "Unknown FriendlyApi engine '$key'";

		parent::__construct($message);
	}
}