<?php

namespace Helium\FriendlyApi\Exceptions;

use InvalidArgumentException;
use Throwable;

class FriendlyApiMethodException extends InvalidArgumentException
{
	/**
	 * @description An unknown or unsupported HTTP method was called
	 * @param string $attempted
	 * @param array $allowed
	 */
	public function __construct(string $attempted, array $allowed)
	{
		$allowedMethods = implode(', ', $allowed);
		$message .= "{$attempted} method is not allowed. Allowed methods: {$allowedMethods}";
	}
}