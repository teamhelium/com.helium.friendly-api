<?php

namespace Helium\FriendlyApi\Models;

/**
 * @description A standard response for all FriendlyApi requests
 * @package Helium\FriendlyApi\Models
 */
class FriendlyApiResponse
{
	protected $code;
	protected $headers;
	protected $body;

	public function __construct(int $code, array $headers, string $body)
	{
		$this->code = $code;
		$this->headers = $headers;
		$this->body = $body;
	}

	/**
	 * @description Get HTTP Status Code
	 * @return int
	 */
	public function getCode(): int
	{
		return $this->code;
	}

	/**
	 * @description Determine if HTTP Status Code is successful
	 * @return bool
	 */
	public function isSuccessfulStatusCode(): bool
	{
		return preg_match('/^2[0-9][0-9]$/', $this->getCode());
	}

	/**
	 * @description Get all response headers
	 * @return array
	 */
	public function getHeaders(): array
	{
		return $this->headers;
	}

	/**
	 * @description Get response body as a raw string
	 * @return string
	 */
	public function getBody(): string
	{
		return $this->body;
	}

	/**
	 * @description Get response body as parsed JSON
	 * @return array
	 */
	public function getJson(): array
	{
		return json_decode($this->getBody(), true) ?? [];
	}

	/**
	 * @description Get array representing this object data
	 * @return array
	 */
	public function toArray(): array
	{
		return [
			'code' => $this->code,
			'headers' => $this->headers,
			'body' => $this->body
		];
	}
}