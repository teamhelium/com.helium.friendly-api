<?php

namespace Helium\FriendlyApi\Engines;

use Helium\FriendlyApi\Contracts\FriendlyApiEngineContract;
use Helium\FriendlyApi\Exceptions\FriendlyApiException;
use Helium\FriendlyApi\Exceptions\FriendlyApiMethodException;
use Helium\FriendlyApi\FriendlyApi;
use Helium\FriendlyApi\Models\FriendlyApiResponse;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Middleware;
use Exception;

/**
 * This class acts as the default engine for FriendlyApi, and is built to directly
 * interact with remote APIs using the popular GuzzleHttp package
 */
class GuzzleFriendlyApiEngine implements FriendlyApiEngineContract
{
	//region Base
	const ALLOWED_METHODS = [
		FriendlyApi::GET,
		FriendlyApi::POST,
		FriendlyApi::PUT,
		FriendlyApi::PATCH,
		FriendlyApi::DELETE
	];

	/** @var GuzzleClient */
	protected $client;

	protected $method;
	protected $baseUrl;
	protected $path;
	protected $headers = [];
	protected $query;
	protected $formParams;
	protected $json;

	/**
	 * @description Construct GuzzleFriendlyApiEngine. Allow injection of custom
	 * Guzzle Client instance (useful for testing/other custom behavior)
	 * @param GuzzleClient|null $client
	 */
	public function __construct(?GuzzleClient $client = null)
	{
		$this->client = $client ?? new GuzzleClient();
	}
	//endregion

	//region Helpers
	/**
	 * @description Get the URL path for the request
	 * @return string
	 */
	protected function getPath()
	{
		$config = $this->client->getConfig();

		//If the guzzle client is pre-configured with a base uri, use it
		//Otherwise, use the base url set in this class
		if (!isset($config['base_uri']) &&
			property_exists($this, 'baseUrl') && $this->baseUrl)
		{
			return "{$this->baseUrl}/{$this->path}";
		}

		return $this->path;
	}

	/**
	 * @description Build and return the array of options for the Guzzle client to
	 * make the request
	 * @return array
	 */
	protected function getOptions()
	{
		$options = [];

		if (property_exists($this, 'json') && $this->json)
		{
			$options['json'] = $this->json;

			if (!isset($this->headers['Content-Type']))
			{
				$this->addHeader(
					'Content-Type', 
					'application/json;charset=utf-8'
				);
			}
		}
		elseif (property_exists($this, 'form_params') && $this->formParams)
		{
			$options['form_params'] = $this->formParams;

			if (!isset($this->headers['Content-Type']))
			{
				$this->addHeader(
					'Content-Type',
					'application/x-www-form-urlencoded;charset=utf-8'
				);
			}
		}

		if (property_exists($this, 'query') && $this->query)
		{
			$options['query'] = $this->query;
		}

		$options['headers'] = $this->headers;

		return $options;
	}
	//endregion

	//region Contract Functions
	/**
	 * @inheritDoc
	 */
	public function setMethod(string $method): FriendlyApiEngineContract
	{
		if (!in_array($method, self::ALLOWED_METHODS))
		{
			throw new FriendlyApiMethodException($method, self::ALLOWED_METHODS);
		}

		$this->method = $method;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setBaseUrl(string $url): FriendlyApiEngineContract
	{
		$this->baseUrl = rtrim($url, '/');
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setPath(string $path): FriendlyApiEngineContract
	{
		$this->path = ltrim($path, '/');
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setHeaders(array $headers): FriendlyApiEngineContract
	{
		$this->headers = $headers;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function addHeader(string $name, string $value): FriendlyApiEngineContract
	{
		$this->headers[$name] = $value;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setQuery(array $query): FriendlyApiEngineContract
	{
		$this->query = $query;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setFormParams(array $formParams): FriendlyApiEngineContract
	{
		$this->formParams = $formParams;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function setJsonQuery(array $data): FriendlyApiEngineContract
	{
		$this->json = $data;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function send(): FriendlyApiResponse
	{
		try
		{
			$response = $this->client->request(
				$this->method,
				$this->getPath(),
				$this->getOptions()
			);
		}
		catch (RequestException $e)
		{
			/**
			 * Guzzle throws an exception for responses with unsuccessful status
			 * codes. Catch it, and respond with a normal FriendlyApiResponse object.
			 */
			$response = $e->getResponse();

			/**
			 * If no response is present, throw an exception.
			 * This may happen if the request is sent bus does not successfully
			 * return, such as in a network timeout
			 */
			if (is_null($response))
			{
				throw new FriendlyApiException($e);
			}
		}
		catch (Exception $e)
		{
			/**
			 * By default, catch and re-throw all other exceptions as a
			 * FriendlyApiException. Include the previous exception for debugging
			 * purposes.
			 */
			throw new FriendlyApiException($e);
		}

		/**
		 * If all else is good, return a standard FriendlyApiResponse object
		 */
		return new FriendlyApiResponse(
			$response->getStatusCode(),
			$response->getHeaders(),
			$response->getBody()->getContents()
		);
	}
	//endregion
}