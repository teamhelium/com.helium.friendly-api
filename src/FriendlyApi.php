<?php

namespace Helium\FriendlyApi;

use Helium\FriendlyApi\Contracts\FriendlyApiEngineContract;
use Helium\FriendlyApi\Engines\GuzzleFriendlyApiEngine;
use Helium\FriendlyApi\Exceptions\FriendlyApiException;
use Helium\FriendlyApi\Models\FriendlyApiResponse;
use Closure;
use Helium\ServiceManager\EngineContract;
use Helium\ServiceManager\ServiceManager;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin FriendlyApiEngineContract
 */
class FriendlyApi extends ServiceManager
{
	public const GET = 'get';
	public const POST = 'post';
	public const PATCH = 'patch';
	public const PUT = 'put';
	public const DELETE = 'delete';

	//region Base
	public function getEngineContract(): string
	{
		return FriendlyApiEngineContract::class;
	}

	protected function createDefaultEngine(): EngineContract
	{
		return new GuzzleFriendlyApiEngine();
	}
	//endregion

	//region Contract
	public function setMethod(string $method): FriendlyApiEngineContract
	{
		return $this->engine()->setMethod($method);
	}

	public function setBaseUrl(string $url): FriendlyApiEngineContract
	{
		return $this->engine()->setBaseUrl($url);
	}

	public function setPath(string $path): FriendlyApiEngineContract
	{
		return $this->engine()->setPath($path);
	}

	public function setHeaders(array $headers): FriendlyApiEngineContract
	{
		return $this->engine()->setHeaders($headers);
	}

	public function addHeader(string $name, string $value): FriendlyApiEngineContract
	{
		return $this->engine()->addHeader($name, $value);
	}

	public function setQuery(array $query): FriendlyApiEngineContract
	{
		return $this->engine()->setQuery($query);
	}

	public function setFormParams(array $formParams): FriendlyApiEngineContract
	{
		return $this->engine()->setFormParams($formParams);
	}

	public function setJsonQuery(array $data): FriendlyApiEngineContract
	{
		return $this->engine()->setJsonQuery($data);
	}

	public function send(): FriendlyApiResponse
	{
		return $this->engine()->send();
	}
	//endregion
}