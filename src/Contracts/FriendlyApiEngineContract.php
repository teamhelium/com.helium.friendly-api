<?php

namespace Helium\FriendlyApi\Contracts;

use Helium\FriendlyApi\Models\FriendlyApiResponse;
use Helium\ServiceManager\EngineContract;

interface FriendlyApiEngineContract extends EngineContract
{
	/**
	 * @description Set the request method
	 * @param string $method
	 * @return static
	 */
	public function setMethod(string $method): FriendlyApiEngineContract;

	/**
	 * @description Set the request base URL
	 * @param string $url
	 * @return static
	 */
	public function setBaseUrl(string $url): FriendlyApiEngineContract;

	/**
	 * @description Set the request path
	 * @param string $path
	 * @return static
	 */
	public function setPath(string $path): FriendlyApiEngineContract;

	/**
	 * @description Set all outgoing request headers
	 * @param array $headers An associative array of headers
	 * @return static
	 */
	public function setHeaders(array $headers): FriendlyApiEngineContract;

	/**
	 * @description Add a single outgoing request header
	 * @param string $name Header name
	 * @param string $value Header value
	 * @return static
	 */
	public function addHeader(string $name,
		string $value): FriendlyApiEngineContract;

	/**
	 * @description Set query data to be encoded into the request url
	 * @param array $query Associative array of query data
	 * @return static
	 */
	public function setQuery(array $query): FriendlyApiEngineContract;

	/**
	 * @description Set form params for request
	 * @param array $formParams
	 * @return FriendlyApiEngineContract
	 */
	public function setFormParams(array $formParams): FriendlyApiEngineContract;

	/**
	 * @description Set query data to be encoded as a JSON request body
	 * @param array $data
	 * @return static
	 */
	public function setJsonQuery(array $data): FriendlyApiEngineContract;

	/**
	 * @description Send the request, and return the response in a friendly format
	 * @return FriendlyApiResponse
	 */
	public function send(): FriendlyApiResponse;
}