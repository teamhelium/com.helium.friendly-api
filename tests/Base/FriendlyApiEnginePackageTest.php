<?php

namespace Helium\FriendlyApi\Tests\Base;

use Helium\FriendlyApi\Contracts\FriendlyApiEngineContract;
use Helium\FriendlyApi\FriendlyApi;
use Orchestra\Testbench\TestCase;

abstract class FriendlyApiEnginePackageTest extends TestCase
{
	protected abstract function getInstance(): FriendlyApiEngineContract;

	public function testSetMethodReturnsSelf()
	{
		$engine = $this->getInstance();

		$this->assertEquals(
			$engine,
			$engine->setMethod(FriendlyApi::GET)
		);
	}

	public function testSetUrlReturnsSelf()
	{
		$engine = $this->getInstance();

		$this->assertEquals(
			$engine,
			$engine->setPath('http://google.com')
		);
	}

	public function testSetHeadersReturnsSelf()
	{
		$engine = $this->getInstance();

		$this->assertEquals(
			$engine,
			$engine->setHeaders([])
		);
	}

	public function testAddHeaderReturnsSelf()
	{
		$engine = $this->getInstance();

		$this->assertEquals(
			$engine,
			$engine->addHeader('Key', 'Value')
		);
	}

	public function testSetQueryReturnsSelf()
	{
		$engine = $this->getInstance();

		$this->assertEquals(
			$engine,
			$engine->setQuery([])
		);
	}

	public function testSetJsonQueryReturnsSelf()
	{
		$engine = $this->getInstance();

		$this->assertEquals(
			$engine,
			$engine->setJsonQuery([])
		);
	}
}