<?php

namespace Helium\FriendlyApi\Tests;

use Helium\FriendlyApi\Contracts\FriendlyApiEngineContract;
use Helium\FriendlyApi\Engines\GuzzleFriendlyApiEngine;
use Helium\FriendlyApi\Exceptions\FriendlyApiException;
use Helium\FriendlyApi\FriendlyApi;
use Helium\FriendlyApi\Models\FriendlyApiResponse;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Helium\FriendlyApi\Tests\Base\FriendlyApiEnginePackageTest;

class GuzzleFriendlyApiEnginePackageTest extends FriendlyApiEnginePackageTest
{
	protected function getMock(): MockHandler
	{
		return new MockHandler();
	}

	protected function getInstance(
		?MockHandler $handler = null): FriendlyApiEngineContract
	{
		$client = null;

		if ($handler)
		{
			$stack = HandlerStack::create($handler);

			$config = [
				'handler' => $stack
			];

			$client = new GuzzleClient($config);
		}

		return new GuzzleFriendlyApiEngine($client);
	}

	public function testSendSuccessReturnsFriendlyApiResponse()
	{
		$mock = $this->getMock();
		$mock->append(new Response(
			200,
			[
				'Content-Type' => 'application/json'
			],
			json_encode([
				'abc' => 123
			])
		));

		$engine = $this->getInstance($mock);

		$response = $engine->setMethod(FriendlyApi::GET)
			->setBaseUrl('http://api.heliumservices.com')
			->setPath('test')
			->send();

		$this->assertInstanceOf(FriendlyApiResponse::class, $response);
		$this->assertEquals(200, $response->getCode());
		$this->assertArrayHasKey('Content-Type', $response->getHeaders());
		$this->assertEquals($response->getHeaders()['Content-Type'][0], 'application/json');
		$this->assertEquals($response->getBody(), json_encode(['abc' => 123]));
		$this->assertEquals($response->getJson(), ['abc' => 123]);
	}

	public function testSendRequestExceptionReturnsFriendlyApiResponse()
	{
		$mock = $this->getMock();
		$mock->append(new RequestException(
			'Internal Server Error',
			new Request(
				FriendlyApi::GET,
				'http://api.heliumservices.com'
			),
			new Response(
				500,
				[
					'Content-Type' => 'application/json'
				],
				json_encode([
					'error_message' => 'Something went wrong'
				])
			)
		));

		$engine = $this->getInstance($mock);

		$response = $engine->setMethod(FriendlyApi::GET)
			->setBaseUrl('http://api.heliumservices.com')
			->setPath('test')
			->send();

		$this->assertInstanceOf(FriendlyApiResponse::class, $response);
		$this->assertEquals(500, $response->getCode());
		$this->assertArrayHasKey('Content-Type', $response->getHeaders());
		$this->assertEquals($response->getHeaders()['Content-Type'][0], 'application/json');
		$this->assertEquals($response->getBody(), json_encode(['error_message' => 'Something went wrong']));
		$this->assertEquals($response->getJson(), ['error_message' => 'Something went wrong']);
	}

	public function testSendConnectExceptionThrowsFriendlyApiException()
	{
		$mock = $this->getMock();

		$exception = new ConnectException(
			'Connection Failed',
			new Request(
				FriendlyApi::GET,
				'http://api.heliumservices.com'
			)
		);

		$mock->append($exception);

		$engine = $this->getInstance($mock);

		try {
			$response = $engine->setMethod(FriendlyApi::GET)
				->setBaseUrl('http://api.heliumservices.com')
				->setPath('test')
				->send();

			$this->assertTrue(false);
		}
		catch (Exception $e)
		{
			$this->assertInstanceOf(FriendlyApiException::class, $e);
			$this->assertEquals($e->getPrevious(), $exception);
		}
	}

	public function testSendOtherExceptionThrowsFriendlyApiException()
	{
		$mock = $this->getMock();

		$exception = new Exception('Some other exception');

		$mock->append($exception);

		$engine = $this->getInstance($mock);

		try {
			$response = $engine->setMethod(FriendlyApi::GET)
				->setBaseUrl('http://api.heliumservices.com')
				->setPath('test')
				->send();

			$this->assertTrue(false);
		}
		catch (Exception $e)
		{
			$this->assertInstanceOf(FriendlyApiException::class, $e);
			$this->assertEquals($e->getPrevious(), $exception);
		}
	}
}
