<?php

namespace Helium\FriendlyApi\Tests\Fakes;

use Helium\FriendlyApi\Contracts\FriendlyApiEngineContract;
use Helium\FriendlyApi\Models\FriendlyApiResponse;

class FakeFriendlyApiEngine implements FriendlyApiEngineContract
{
	public function setMethod(string $method): FriendlyApiEngineContract
	{
		return $this;
	}

	public function setBaseUrl(string $url): FriendlyApiEngineContract
	{
		return $this;
	}

	public function setPath(string $path): FriendlyApiEngineContract
	{
		return $this;
	}

	public function setHeaders(array $headers): FriendlyApiEngineContract
	{
		return $this;
	}

	public function addHeader(string $name, string $value): FriendlyApiEngineContract
	{
		return $this;
	}

	public function setQuery(array $query): FriendlyApiEngineContract
	{
		return $this;
	}

	public function setFormParams(array $formParams): FriendlyApiEngineContract
	{
		return $this;
	}

	public function setJsonQuery(array $data): FriendlyApiEngineContract
	{
		return $this;
	}

	public function send(): FriendlyApiResponse
	{
		return new FriendlyApiResponse(200, [], '');
	}
}