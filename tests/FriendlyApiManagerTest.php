<?php

namespace Helium\FriendlyApi\Tests;

use Helium\FriendlyApi\Engines\GuzzleFriendlyApiEngine;
use Helium\FriendlyApi\Exceptions\FriendlyApiUnknownEngineException;
use Helium\FriendlyApi\FriendlyApi;
use Helium\FriendlyApi\Models\FriendlyApiResponse;
use Exception;
use Helium\FriendlyApi\Tests\Fakes\FakeFriendlyApiEngine;
use Helium\ServiceManager\EngineContract;
use Helium\ServiceManager\ServiceManager;
use Helium\ServiceManager\Tests\Base\ServiceManagerPackageTest;
use Orchestra\Testbench\TestCase;

class FriendlyApiManagerTest extends ServiceManagerPackageTest
{
	protected function getInstance(): ServiceManager
	{
		$manager = new FriendlyApi('fake');

		$manager->extend('fake', $this->getNewEngine());

		return $manager;
	}

	protected function getNewEngine(): EngineContract
	{
		return new FakeFriendlyApiEngine();
	}

	public function testPassthroughReturnsExpected()
	{
		$manager = $this->getInstance();

		$this->assertEquals($manager->engine(), $manager->setMethod(FriendlyApi::GET));

		$this->assertEquals(
			$manager->engine(),
			$manager->setBaseUrl('http://api.heliumservices.com')
		);

		$this->assertEquals($manager->engine(), $manager->setPath('test'));

		$this->assertEquals($manager->engine(), $manager->setHeaders([]));

		$this->assertEquals(
			$manager->engine(),
			$manager->addHeader('Conent-Type', 'application/json')
		);

		$this->assertEquals($manager->engine(), $manager->setQuery([]));

		$this->assertEquals($manager->engine(), $manager->setFormParams([]));

		$this->assertEquals($manager->engine(), $manager->setJsonQuery([]));

		$this->assertInstanceOf(FriendlyApiResponse::class, $manager->send());
	}
}
